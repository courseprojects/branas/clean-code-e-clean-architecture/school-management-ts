import ValidDate from "../value_objects/ValidDate"
import Cpf from "../value_objects/Cpf"
import Name from "../value_objects/Name"

export default class Student {
    name: Name
    cpf: Cpf
    birthDate: ValidDate

    constructor (name: string, cpf: string, birthDate:string ) {
        this.name = new Name(name)
        this.cpf = new Cpf(cpf)
        this.birthDate = new ValidDate(birthDate)
    }

    getAge () : Number {
        return new Date().getFullYear() - this.birthDate.value.getFullYear()
    }
}