export default class Cpf {
  value: string
  constructor (cpf:string) {
    const regexCpf = /^([\d]{3}[\.]?){2}[\d]{3}[\-]?[\d]{2}$/
    if (!regexCpf.test(cpf)) throw new Error('Invalid cpf.')
    if (!Cpf.isValid(cpf)) throw new Error('Invalid cpf.')
    this.value = cpf
  }

  static clean(cpf:string) {
    return cpf.replace(/\D/g, '')
  }

  static hasCorrectLenght(cpf:string) {
    return cpf.length == 11
  }

  static isAllowed(cpf:string) {
    const firstDigit = cpf[0]
    return !cpf.split('').every(digit => digit === firstDigit)
  }

  static generateFactories(factory:number) {
    return Array.from(Array(factory + 1).keys()).slice(2).reverse()
  }

  static calcVerifyDigit(cpf:string, isFirstDigit:boolean = true) {
    const cpfArray = cpf.split('').map(digit => parseInt(digit))
    let resultSum = 0
    const max = isFirstDigit ? 10 : 11
    const factories = this.generateFactories(max)
    factories.forEach((factory, index) => {
      resultSum += cpfArray[index] * factory
    })
    const rest = resultSum % 11
    if (rest < 2 || rest > 10) return 0
    return 11 - rest
  }

  static isValid(cpf:string) {
    const cleanedCpf = this.clean(cpf)
    if (!this.hasCorrectLenght(cleanedCpf)) return false
    if (!this.isAllowed(cleanedCpf)) return false
    const firstVerificatedDigit = this.calcVerifyDigit(cleanedCpf)
    const lastVerificatedDigit = this.calcVerifyDigit(cleanedCpf, false)
    return cleanedCpf.slice(9) === `${firstVerificatedDigit}${lastVerificatedDigit}`
  }
}