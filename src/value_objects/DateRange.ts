import ValidDate from "./ValidDate"

const _MILLISECONDS_PER_DAY = 1000*60*60*24

export default class DateRange {
    startDate: Date
    endDate: Date
    _millisecondsPerDay: number

    constructor (startDate:string|Date, endDate:string|Date) {
        this._millisecondsPerDay = _MILLISECONDS_PER_DAY
        this.startDate = typeof startDate === "string" ? new ValidDate(startDate).value : startDate
        this.endDate = typeof endDate === "string" ? new ValidDate(endDate).value : endDate
        if (endDate < startDate) throw new Error('Invalid date interval.')
    }

    countDays() : number {
        return (this.endDate.getTime() - this.startDate.getTime()) / this._millisecondsPerDay
    }

    

}