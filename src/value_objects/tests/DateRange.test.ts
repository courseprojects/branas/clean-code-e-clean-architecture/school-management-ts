import DateRange from "../DateRange";

test('should invalid date interval', () => {
    const startDate = '2021-01-10'
    const endDate = '2021-01-01'
    expect(()=> new DateRange(startDate, endDate)).toThrow('Invalid date interval.')
});


test('should valid date interval', () => {
    const startDate = '2021-01-01'
    const endDate = '2021-01-10'
    expect(()=> new DateRange(startDate, endDate)).not.toThrow('Invalid date interval.')
});

test('should have a date interval equal ten', () => {
    const startDate = '2021-01-01'
    const endDate = '2021-01-11'
    const range = new DateRange(startDate, endDate)
    expect(range.countDays()).toBe(10)
});