import ValidDate from "../ValidDate";


test('should invalid birth date format', () => {
    expect(() => new ValidDate('03-12-2000')).toThrow('Invalid date format.')
});

test('should invalid birth date', () => {
    expect(() => new ValidDate('0000-99-88')).toThrow('Invalid date')
});

test('should valid birth date', () => {
    const validDate = new ValidDate('2021-01-02')
    expect(validDate.value.getDate()).toBe(new Date('2021-01-02').getDate())
});