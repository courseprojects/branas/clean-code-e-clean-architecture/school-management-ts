import Cpf from '../Cpf'

test('should invalid cpf string on regex', () => {
    expect(() => new Cpf('lkjldfjaldkf')).toThrow('Invalid cpf.')
});

test('should invalid cpf number', () => {
    expect(() => new Cpf('12345678987')).toThrow('Invalid cpf.')
});

test('should asset first verification digit', () => {
    const validCpfWithoutMask = '62054386073'
    expect(Cpf.calcVerifyDigit(validCpfWithoutMask)).toBe(7)
});

test('should asset last verification digit', () => {
    const validCpfWithoutMask = '62054386073'
    expect(Cpf.calcVerifyDigit(validCpfWithoutMask, false)).toBe(3)
});