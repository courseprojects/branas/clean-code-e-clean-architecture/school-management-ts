import Name from "../Name";


test('should invalid name', () => {
    expect(() => new Name('Irineu')).toThrow('Invalid name.')
});

test('should valid name', () => {
    const name = new Name('Irineu jubileu')
    expect(name.value).toBe('Irineu jubileu')
});