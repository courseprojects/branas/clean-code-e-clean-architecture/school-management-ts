export default class ValidDate {
    value: Date
    constructor (date:string) {
        const regexDate = /^[\d]{4}\-[\d]{2}\-[\d]{2}(\T[\d]{2}\:[\d]{2}\:[\d]{2}\.[\d]{3}\Z)?$/
        if (!regexDate.test(date)) throw new Error('Invalid date format.')
        const validDate = new Date(date)
        if(isNaN(validDate.getTime())) throw new Error('Invalid date.')
        this.value = validDate
    }
}