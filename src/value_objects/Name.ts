export default class Name {
    value: string
    
    constructor (name:string) {
        const regexName = /([A-Za-z]+ )+([A-Za-z])+$/
        if (!regexName.test(name)) throw new Error('Invalid name.');
        this.value = name;
    }
}