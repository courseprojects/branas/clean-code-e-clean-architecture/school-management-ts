import DatabaseRepositoryInterface from '../user_cases/interfaces/DataBaseRepository'
import DbInterface from '../user_cases/interfaces/DbInterface'
import Class from '../user_cases/types/Class'
import Level from '../user_cases/types/Level'
import Module from '../user_cases/types/Module'

export default class DbRespository implements DatabaseRepositoryInterface {

    database: DbInterface

    constructor(database:DbInterface) {
        this.database = database
    }
    
    getLevel(levelCode: string) : Level {
        const level = this.database.levels.find(level => level.code === levelCode)
        if (!level) throw new Error('Level not found.')
        return level
    }

    getModule(levelCode: string, moduleCode: string) : Module {
        const module = this.database.modules.find(md => md.code === moduleCode && md.level === levelCode)
        if (!module) throw new Error('Module not found.')
        return module
    }

    getClass(levelCode: string, moduleCode: string, classCode: string) : Class {
        const classe = this.database.classes.find(cls => {
            return cls.code === classCode && cls.level === levelCode && cls.module === moduleCode
        })
        if (!classe) throw new Error('Class not found.')
        return classe
    }
 
    
}
