import ClassRepositoryInterface from "../../user_cases/interfaces/ClassRepositoryInterface";
import Class from "../../user_cases/types/Class";
import DateRange from "../../value_objects/DateRange";

export default class ClassRepository implements ClassRepositoryInterface {
    classes: Class[];

    constructor() {
        this.classes = [
            {
                level: "EM",
                module: "3",
                code: "A",
                capacity: 10,
                start_date: "2021-04-01",
                end_date: "2021-04-30"
            },
            {
                level: "EM",
                module: "1",
                code: "A",
                capacity: 1,
                start_date: "2021-05-01",
                end_date: "2021-05-30"
            },
            {
                level: "EM",
                module: "3",
                code: "A",
                capacity: 1,
                start_date: "2021-06-01",
                end_date: "2021-12-15"
            },
            {
                level: "EM",
                module: "3",
                code: "B",
                capacity: 1,
                start_date: "2021-05-01",
                end_date: "2021-05-30"
            },
            {
                level: "EM",
                module: "3",
                code: "C",
                capacity: 1,
                start_date: "2021-04-20",
                end_date: "2021-05-31"
            }
        ]
    }
    hasFinishedClass(clazz: Class): boolean {
        return new Date() > new Date(clazz.end_date)
    }
    isOpenToEnroll(clazz: Class): boolean {
        // Receber DateRange por injeção de dependencia? :/
        const MAXIMUM_PERCENTAGE_OF_WORKLOAD_ALLOWED = 25
        const totalClassDays = new DateRange(clazz.start_date, clazz.end_date).countDays()
        const daysOfClassStarted = new DateRange(clazz.start_date, new Date()).countDays()
        const percentageOfWorkload = (daysOfClassStarted * 100) / totalClassDays
        return percentageOfWorkload <= MAXIMUM_PERCENTAGE_OF_WORKLOAD_ALLOWED
    }

    find(code: string, level:string, module:string): Class {
        const classe = this.classes.find(clazz => {
            return clazz.code === code && clazz.level === level && clazz.module === module
        })
        if (!classe) throw new Error('Class not found.')
        return classe
    }
    
}