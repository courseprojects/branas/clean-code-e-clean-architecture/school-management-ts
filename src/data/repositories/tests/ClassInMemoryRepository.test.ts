import ClassInMemoryRepository from "../ClassInMemoryRepository";


test('should not is open class to enroll', () => {
    const repository = new ClassInMemoryRepository()
    const clazz = { ...repository.classes[0], start_date: '2021-05-01', end_date: '2021-05-10' }
    expect(repository.isOpenToEnroll(clazz)).toBe(false)
});

test('should is open class to enroll', () => {
    const repository = new ClassInMemoryRepository()
    // TODO mocar start e end date para a diferença correta para a data atual
    const clazz = { ...repository.classes[0], start_date: '2021-06-01', end_date: '2021-06-30' }
    expect(repository.isOpenToEnroll(clazz)).toBe(true)
});