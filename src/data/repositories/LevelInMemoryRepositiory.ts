import LevelRepositoryInterface from "../../user_cases/interfaces/LevelRepositoryInterface";
import Level from "../../user_cases/types/Level";

export default class LevelRepository implements LevelRepositoryInterface {
    levels: Level[];

    constructor() {
        this.levels = [
            {
                code: "EF1",
                description: "Ensino Fundamental I"
            },
            {
                code: "EF2",
                description: "Ensino Fundamental II"
            },
            {
                code: "EM",
                description: "Ensino Médio"
            }
        ]
    }

    find(code: string): Level {
        const level = this.levels.find(level => level.code === code)
        if (!level) throw new Error('Level not found.')
        return level
    }
    
}