import Student from "../../entities/Student"
import Enrollment from "../../entities/Enrollment"

import LevelRepositoryInterface from "../interfaces/LevelRepositoryInterface"
import ModuleRepositoryInterface from "../interfaces/ModuleRepositoryInterface"
import ClassRepositoryInterface from "../interfaces/ClassRepositoryInterface"
import EnrollmentRepositoryInterface from "../interfaces/EnrollRepositoryInterface"
import Module from "../types/Module"
import Invoice from "../../entities/Invoice"


export default class EnrollStudent {
    enrollments: Enrollment[]
    levelRepository: LevelRepositoryInterface
    moduleRepository: ModuleRepositoryInterface
    classRepository: ClassRepositoryInterface
    enrollRepository: EnrollmentRepositoryInterface

    constructor (
        levelRepository:LevelRepositoryInterface,
        moduleRepository:ModuleRepositoryInterface,
        classRepository:ClassRepositoryInterface,
        enrollRepository: EnrollmentRepositoryInterface
        ) {
        this.enrollments = []
        this.levelRepository = levelRepository
        this.moduleRepository = moduleRepository
        this.classRepository = classRepository
        this.enrollRepository = enrollRepository
    }

    generateEnrollmentCode(classCode:string, levelCode:string, moduleCode:string) {
        const currentYear = new Date().getFullYear().toString()
        const currentSequenceCode = (this.enrollments.length + 1).toString().padStart(4, '0')
        return ''.concat(currentYear, levelCode, moduleCode, classCode, currentSequenceCode)
    }

    generateInvoices(module:Module, installments:number) : Invoice[] {
        if (installments === 1) return [new Invoice(module.price)]
        const installmentAmount = Math.floor(module.price / installments)
        const lastInstallmentAmount = (module.price - ( installmentAmount * (installments -1)))
        const invoices = Array.from(new Array(installments - 1).keys()).map(()=> new Invoice(installmentAmount))
        invoices.push(new Invoice(lastInstallmentAmount))
        return invoices
    }

    execute (enrollRequest:any) {
        const { name, cpf, birthDate } = enrollRequest.student
        const student = new Student(name, cpf, birthDate)
        const { level: levelCode, module: moduleCode, class: classCode } = enrollRequest
        const level = this.levelRepository.find(levelCode)
        const module = this.moduleRepository.find(moduleCode, levelCode)
        const clazz = this.classRepository.find(classCode, levelCode, moduleCode)
        if (this.classRepository.hasFinishedClass(clazz)) throw new Error('Class is already finished.')
        if (!this.classRepository.isOpenToEnroll(clazz)) throw new Error('Class is already started.')
        const studentsInClass = this.enrollRepository.findAllByClass(level.code, module.code, clazz.code)
        if (!(clazz.capacity > studentsInClass.length)) throw new Error('Class is over capacity.')
        if (student.getAge() <  module.minimumAge) throw new Error('Student below minimum age.')
        const existingEnrollment = this.enrollRepository.findByCpf(student.cpf.value)
        if (existingEnrollment) throw new Error('Enrollment with duplicated student is not allowed.')
        const enroll = new Enrollment(
            student,level.code, module.code, clazz.code,
            this.generateEnrollmentCode(clazz.code, level.code, module.code),
            this.generateInvoices(module, 12)
        )
        this.enrollRepository.save(enroll)
        return enroll
    }

}