import ClassInMemoryRepository from "../../data/repositories/ClassInMemoryRepository";
import LevelInMemoryRepository from "../../data/repositories/LevelInMemoryRepositiory";
import ModuleInMemoryRepository from "../../data/repositories/ModuleInMemoryRepository";
import EnrollmentInMemoryRepository from "../repositories/EnrollmentInMemoryRepository";
import EnrollStudent from "./EnrollStudent";


function enrollStudentFactory () : EnrollStudent {
  const levelRepository = new LevelInMemoryRepository()
  const moduleRepository = new ModuleInMemoryRepository()
  const classRepository = new ClassInMemoryRepository()
  const enrollRepository = new EnrollmentInMemoryRepository()
  return new EnrollStudent(levelRepository, moduleRepository, classRepository, enrollRepository)
}

let enrollStudent: EnrollStudent;
beforeEach(function () {
  jest.useFakeTimers('modern').setSystemTime(new Date('2021-05-01').getTime());
  enrollStudent = enrollStudentFactory()
});

test('should no enroll without invalid student name ', () => {
  const enrollRequest = {
    student: {
      name: "Ana"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  expect(() => enrollStudent.execute(enrollRequest)).toThrow('Invalid name.')
});

test('should no enroll without invalid student cpf ', () => {
  const enrollRequest = {
    student: {
      name: "Ana luisa",
      cpf: "000000"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  expect(() => enrollStudent.execute(enrollRequest)).toThrow('Invalid cpf.')
});

test('should no enroll without valid student cpf ', () => {
  const enrollRequest = {
    student: {
      name: "Ana luisa",
      cpf: "620.543.860-73",
      birthDate: "2002-03-12"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  enrollStudent.execute(enrollRequest)
  expect(enrollStudent.enrollRepository.count()).toBe(1)
});

test('Should generate enrollment code', () => {
  const enrollRequest = {  
    student: {
        name: "Maria Carolina Fonseca",
        cpf: "755.525.774-26",
        birthDate: "2002-03-12"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  const expecteEnrollmentCode = '2021EM1A0001'
  const enroll = enrollStudent.execute(enrollRequest)
  expect(enroll.code).toBe(expecteEnrollmentCode)
});

test('Should not enroll student below minimum age', () => {
  const enrollRequest = {
    student: {
        name: "Maria Carolina Fonseca",
        cpf: "755.525.774-26",
        birthDate: "2007-03-12"
    },
    level: "EM",
    module: "1",
    class: "A"
}
  expect(() => enrollStudent.execute(enrollRequest)).toThrow('Student below minimum age.')
});

test('Should not enroll student over class capacity', () => {
  const enrollRequest1 = {
    student: {
      name: "Ana Silva",
      cpf: "832.081.519-34",
      birthDate: "2002-03-12"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  const enrollRequest2 = {
    student: {
      name: "Maria Carolina Fonseca",
      cpf: "755.525.774-26",
      birthDate: "2002-03-12"
    },
    level: "EM",
    module: "1",
    class: "A"
  }
  enrollStudent.execute(enrollRequest1)
  expect(() => enrollStudent.execute(enrollRequest2)).toThrow('Class is over capacity.')
});


test('Should not enroll after the end of the class', () => {
  const enrollRequest = {
    student: {
        name: "Maria Carolina Fonseca",
        cpf: "755.525.774-26",
        birthDate: "2002-03-12"
    },
    level: "EM",
    module: "3",
    class: "A",
  }

  expect(() => enrollStudent.execute(enrollRequest)).toThrow('Class is already finished.')
})

test('Should not enroll after 25% of the start of the class', () => {
  const enrollRequest = {
    student: {
        name: "Maria Carolina Fonseca",
        cpf: "755.525.774-26",
        birthDate: "2002-03-12"
    },
    level: "EM",
    module: "3",
    class: "C",
  }

  expect(() => enrollStudent.execute(enrollRequest)).toThrow('Class is already started.')
})


test('Should generate the invoices based on the number of installments, rounding each amount and applying the rest in the last invoice', () => {
  const enrollRequest = {
    student: {
        name: "Maria Carolina Fonseca",
        cpf: "755.525.774-26",
        birthDate: "2002-03-12"
    },
    level: "EM",
    module: "1",
    class: "A",
    installments: 12
  }
  const enroll = enrollStudent.execute(enrollRequest)
  expect(enroll.invoices.length).toBe(12)
  const amount = enroll.invoices.map(invoice => invoice.amount).reduce((a, b) => a + b)
  expect(amount).toBe(17000)
})