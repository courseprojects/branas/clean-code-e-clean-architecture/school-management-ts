import Class from "../types/Class";
import Level from "../types/Level";
import Module from "../types/Module";


export default interface DbInterface {
    levels: Level[]
    modules: Module[]
    classes: Class[]
}