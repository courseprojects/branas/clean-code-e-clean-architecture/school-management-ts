import DbInterface from './DbInterface';
import Class from '../types/Class';
import Level from '../types/Level';
import Module from '../types/Module'

export default interface DataBaseRepositoryInterface  {
    database: DbInterface
    
    getLevel (levelCode:string) : Level
    
    getModule (levelCode:string, moduleCode:string) : Module
    
    getClass (levelCode:string, moduleCode:string, classCode:string) : Class

}