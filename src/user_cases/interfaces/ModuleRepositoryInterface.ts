import ModuleType from '../types/Module'

export default interface ModuleRepositoryInterface {
    modules:ModuleType[]

    find(code:string, level:string) : ModuleType
}