import LevelType from '../types/Level'

export default interface LevelRepositoryInterface {
    levels:LevelType[]

    find(code:string) : LevelType
}