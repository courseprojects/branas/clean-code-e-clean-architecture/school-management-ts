import ClassType from '../types/Class'

export default interface ClassRepositoryInterface {
    classes:ClassType[]

    find(code:string, level:string, module:string) : ClassType

    isOpenToEnroll(clazz: ClassType) : boolean

    hasFinishedClass(clazz: ClassType) : boolean
}