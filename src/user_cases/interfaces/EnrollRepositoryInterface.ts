import Enrollment from "../../entities/Enrollment";

export default interface EnrollmentRepositoryInterface {
    save(enrollment: Enrollment): void;
    findAllByClass(level: string, module: string, clazz: string): any;
    findByCpf(cpf: string): any;
    count(): number;
}