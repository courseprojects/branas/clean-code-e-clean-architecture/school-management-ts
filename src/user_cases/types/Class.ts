type Class = {
    level: string
    module: string
    code: string
    capacity: number
    start_date: string
    end_date: string
}

export default Class