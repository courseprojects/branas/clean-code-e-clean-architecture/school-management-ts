type Level = {
    code: string
    description: string
}

export default Level