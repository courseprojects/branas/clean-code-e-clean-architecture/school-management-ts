type Module  = {
    level: string
    code: string
    description: string
    minimumAge: Number
    price: number
}

export default Module