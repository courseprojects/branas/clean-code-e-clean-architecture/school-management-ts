import Enrollment from "../../entities/Enrollment";
import EnrollmentRepositoryInterface from "../interfaces/EnrollRepositoryInterface";


export default class EnrollmentInMemoryRepository implements EnrollmentRepositoryInterface {
    enrollments: Enrollment[]

    constructor() {
        this.enrollments = []
    }

    save(enroll: Enrollment): void {
        this.enrollments.push(enroll)
    }
    findAllByClass(level: string, module: string, clazz: string) {
        return this.enrollments.filter(enroll => {
            return enroll.clazz === clazz && enroll.level === level && enroll.module === module
        })
    }
    findByCpf(cpf: string) {
        return this.enrollments.find(enroll => enroll.student.cpf.value === cpf)
    }
    count(): number {
        return this.enrollments.length
    }
    
}